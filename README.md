
## Docker compose
The `docker run ` and `docker build` commands quickly become super painful to execute. the argument list quickly becomes
unmanageable. Instead, use `docker compose` but always keep in mind what is actually happening under the hood.
```shell
docker build --file api.Dockerfile --build-arg blabetiblou ...
# can be simplified way simpler:
docker compose build api

docker run --env DB_PASSWORD=password ...
# can be simplified
docker compose run api
```

Also, by default, `docker compose` command will load any `.env` file found in the current directory.
=> no more tedious `export` or `source` commands.
```shell
# .env
VCODEBOX_USERNAME_FROM_ENV=usernAm5
VCODEBOX_PASSWORD_FROM_ENV=Passw0rd
DB_PASSWORD_FROM_ENV=blabetibl0u
```
```yml
# docker-compose.yml
services:
  api: 
    build:
      context: .
      dockerfile: api.Dockerfile
      args:  #buildargs
        VCODEBOX_USERNAME: ${VCODEBOX_USERNAME_FROM_ENV}
        VCODEBOX_PASSWORD: ${VCODEBOX_PASSWORD_FROM_ENV}
        http_proxy: ${http_proxy}
        https_proxy: ${https_proxy}
    environment:
      DB_PASSWORD: ${DB_PASSWORD_FROM_ENV}
      DB_USERNAME: username
    volumes:
      - config.ini:/etc/config/config.ini
  gui: #
    build:
      context: .
      dockerfile: gui.Dockerfile
      args:
        VCODEBOX_USERNAME: ${VCODEBOX_USERNAME_FROM_ENV}
        VCODEBOX_PASSWORD: ${VCODEBOX_PASSWORD_FROM_ENV}
        http_proxy: ${http_proxy}
        https_proxy: ${https_proxy}
      environment:
        DIEGO_SCRAT_HOST: ${DIEGO_SCRAT_HOST} # vworker-dev.xxxx.eu.int
  db:
    image: postgres
    environment:
      POSTGRES_PASSWORD: password
      POSTGRES_USER: username
```

## Image creation
### Build behind a proxy
Docker build instructions often includes external imports
```Dockerfile
RUN apt-get update && apt-get install -y other-lib
RUN pip install my-package
RUN npm install other-package
RUN curl https://external-resource.com
```
When behind a proxy, all those commands requires proxy settings to passed to **build phase**.
If the proxy was already available in the env, it's only a matter of forwarding it to the build command.
```shell
docker build \
  --build-arg http_proxy=${http_proxy} \
  --build-arg https_proxy=${https_proxy} \
  --file Dockerfile .
```

### Load internal git dependency
Some projects depend on pieces of code that are only distributed via a SSH internal git.
There is no package registry, no HTTP git. The dependency must be loaded using SSH git
```Dockerfile
RUN git clone ssh://username@hg.xxx.eu.int:/opt/SNet/scm/SNet_Intranet/project /opt/project
# freeze a given version. NEVER trust latest
RUN git -C /opt/project checkout a5d2987aafc14a0400d799973b810d11b06ac374
```
Since we don't want to take the risk to store a private SSH key inside the image, better to pass the user password using `sshpass`
```Dockerfile
# Dockerfile
ARG VCODEBOX_PASSWORD
ARG VCODEBOX_USERNAME

RUN apt-get update && apt-get install -y sshpass
RUN mkdir -p ~/.ssh/ && ssh-keyscan -H hg.xxx.eu.int >> ~/.ssh/known_hosts
RUN sshpass -p ${VCODEBOX_PASSWORD} \
 git clone ssh://${VCODEBOX_USERNAME}@hg.xxx.eu.int:/opt/SNet/scm/SNet_Intranet/project /opt/project
# freeze a given version. NEVER trust latest
RUN git -C /opt/project checkout a5d2987aafc14a0400d799973b810d11b06ac374
```
```shell
export VCODEBOX_PASSWORD=Passw0rd!
export VCODEBOX_USERNAME=username
docker build \
  --build-arg VCODEBOX_PASSOWRD=${VCODEBOX_PASSWORD} \
  --build-arg VCODEBOX_USERNAME=${VCODEBOX_USERNAME}
  --file Dockerfile .
```

### Large configuration files
We strongly promote the usage of environment variable to provide unit settings parameters
```shell
docker run -e DB_PASSWORD='passwrd' my-image
```
But when the application is still using big config file, those sensitive config file
must stay out of the image content, and only provided at run time.
```shell
ls
> config.ini
docker run --volume ${PWD}/config.ini:/etc/config/config.ini my-image
```

> **IMPORTANT** don't forget to add those config files in gitignore
> ```.gitignore
> # .gitignore
> config.ini
> ```

It is also a good practice to include an example of each config file, with dummy values,
that is not skipped by `.gitignore`. It can be used as documentation in docde
```ini
# config.example.ini
[ section ]
# Here a documentation for the secret
password=password
```
```gitignore
# Example .gitignore to keep .example.ini
*.ini
!.example.ini
```

### Docker image content scan
Projects root often contains sensitive files that are under `.gitignore`. Like `.env` 
with database password. Bug `.gitignore` is not enough for Docker. The following command
will take those files and put it into the image
```Dockerfile
COPY . .
RUN ls -la
> drwxr-xr-x   5 raphaeljoie  staff   160 May 23 09:30 .
> drwxr-xr-x  29 raphaeljoie  staff   928 May 21 15:01 ..
> drwxr-xr-x  12 raphaeljoie  staff   384 May 22 10:13 .env
# oooops, .env was copied in image
```
The alternative is to have a more restricted copy of the files. 
```Dockerfile
COPY thisFile thisFile
COPY thatDirectory thatDirectory
COPY ...
```
But it can quickly become painful to maintain.

Instead, Docker comes with `.dockerignore` to list all the files to ignore
```gitignore
# .dockerignore
.env
```
A Good practice is to add a check at the end of the build to make sure nothing bad
was added in the image. This command anyway doesn't have any impact on the image performances.
```Dockerfile
RUN ls -la
```

`.dockerignore` is also useful to make sure the Docker image is not influenced by local build files.
Example of Python `venv` that may be containing OS specific libraries. `COPY . .` will also take `./venv/` into the 
image. The consequence is that a subsequent `pip install` may not do anything (because everything already in venv). 
And the image will end with a `venv` content that has been built on the host OS.

Python project will therefore often have `venv` in the `.dockerignore`
```gitignore
# .dockerignore
venv/
```

### Multiple services/jobs repository
It can happen that a single repository contains the code of multiple services or jobs. 
Examples:
* multiple services: a web application with the React GUI and Flask API stored in the same project
* multiple jobs: a repo with a signe Python module that is providing the implementation of multiple jobs

> **IMPORTANT** — Don't even try to bundle everything in a single container

It is technically possible to start multiple processes in parallel in a single container. But it is not the goal.
A container = a single main process. Example
* GUI: the process is the Nginx server, started in the foreground, exposing the HTML+CSS+JS assets
* Flask: the process is a gunicorn WSGI web server exposing the Flask application
* Job: The process is only the execution of the related python code. Once executed, the container stops and the 
  orchestrator can free the memory and disk space.

> Example docker-compose with API and GUI in the same repository
> ```yml
> version: '3.5'
> services:
>   gui:
>     build:
>       dockerfile: gui.Dockerfile
>     ports:
>       - "8080:80"
>     ...
>   api:
>     build:
>       dockerfile: api.Dockerfile
>     ports:
>       - "8081:5000"
>     ...
> ```

> Example docker-compose form multiple jobs in the same repository
> ```yml
> version: '3.5'
> services:
>   job1:
>     build:
>       dockerfile: job1.Dockerfile
>     cmd: ...
>     ...
>   job2:
>     build:
>       dockerfile: job2.Dockerfile
>     cmd: ...
>     ...
> ```
> Looks weird to run all the jobs together in a single docker-compose file?
> Recall that the docker-compose file of dev projects are not designed for production deployment. It only serves
> as a structured "documentation as code" support, and can be used in dev/test phase
> ```shell
> # easy execution of job1
> docker-compose up --force-recreate job1
> ```

> TODO
> * Image base

### Local mapping
If an GUI is developed together with an API, the front-end is most likely sending backend requests to the same host
```js
// code executed from https://intragate.truc:443/snet/gui/app
fetch("/snet/api/endpoint")  // will translate into GET https://intragate.truc:443/snet/api/endpoint
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
  });
```
But when the API and GUI are running in separated containers, it cannot be exposed to the same port and therefore
the URL of the API and GUI are not the same anymore! The previous code snippet will fail because the GUI is sending
requests to itself ... to the GUI port instead of the API port.

A solution is to change the code
```js
// somewhere
const backend_fqdn = "https://lab-joierap.machin.truc:8081"
// code executed from https://lab-joierap.machin.truc:8080
fetch(`{backend_fqdn}/snet/api/endpoint`)  // will translate into GET https://lab-joierap.machin.truc:8081/snet/api/endpoint
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
  });
```

But, if we don't want to change the code of the GUI to allow the API/GUI split, a local mapping service can be used instead
![local-mapping.drawio.png](local-mapping.drawio.png)

Deploy a third container with the only purpose to redirect the traffic to this or that container. So that, it appears to 
run behind the same fqdn and port
```yml
version: '3.5'
services:
  mapping:
    image: "traefik:v2.10"
    command:
      - "--api.insecure=true"
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--entrypoints.web.address=:80"
    ports:
      - "8000:80"
      # Traefik dashboard
      - "8080:8080"
  api:
    ...
    labels:
      # Enable traefik mapping for this container
      - "traefik.enable=true"
      # Declare a "gui" router for the "web" entrypoint (port 80 of traefik)
      - "traefik.http.routers.api.entrypoint=web"
      # Redirect any request that is /snet/papi/v1/*
      - "traefik.http.routers.api.rule=Pathprefix(`/snet/papi/v1`)"
  gui:
    ...
    labels:
      # Enable traefik mapping for this container
      - "traefik.enable=true"
      # Declare a "gui" router for the "web" entrypoint (port 80 of traefik)
      - "traefik.http.routers.gui.entrypoint=web"
      # Redirect any request that is not /snet/papi/v1/*
      - "traefik.http.routers.gui.rule=!Pathprefix(`/snet/papi/v1`)"
```

### Production entrypoint
> TODO

### Docker image dev phase

#### 1. don't try to optimize while developing an image
Multi-stage, lightweight bases (Alpine, etc), are for optimisation phase. 
In a stack segmentation phase, the first goal is to isolate.

#### 2. think build steps order
> TODO

## Image execution

### Services dependencies

### Image dev mode vs code dev mode

### CORS


